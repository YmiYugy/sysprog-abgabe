#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv) {
    if(argc < 3) {
        fprintf(stderr, "not enough arguments\n");
        return EXIT_FAILURE;
    }
    int rows = atoi(argv[1]);
    int cols = atoi(argv[2]);
    if(rows <= 0 || cols <= 0) {
        fprintf(stderr, "invalid dimensions\n");
    }
    srand(time(0));
    int i;
    for(i = 0; i < rows; i++){
        int j;
        for(j=0;j < cols; j++){
            if(!(j < cols -1)) {
                printf("%i", (rand() - (int)(RAND_MAX/2))%100);
            } else {
                printf("%i,", (rand() - (int)(RAND_MAX/2))%100);
            }
        }
        if(i < rows-1) {
            printf(";");
        }
    }
    printf("\n");
}