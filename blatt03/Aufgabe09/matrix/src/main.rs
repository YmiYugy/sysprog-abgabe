use std::env;
use std::str::FromStr;
use std::error::Error;
use std::ops::{Mul, Add};
use std::fmt;
use std::fmt::Formatter;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 4 {
        eprintln!("Not enough arguments!");
    }
    let matrix1 = &args[1];
    let operation = &args[2];
    let matrix2 = &args[3];
    let matrix1 = Matrix::from_str(matrix1).unwrap();
    let matrix2 = Matrix::from_str(matrix2).unwrap();

    match operation.as_str() {
        "+" => println!("{}", (matrix1 + matrix2).unwrap()),
        "*" => println!("{}", (matrix1 * matrix2).unwrap()),
        _ => eprintln!("invalid operation")
    }
}


struct Matrix{
    data: Vec<Vec<i64>>,
    rows: usize,
    cols: usize,
}

impl FromStr for Matrix {
    type Err = Box<dyn Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let data: Vec<&str> = s.split(";").collect();
        let data: Vec<Vec<&str>> = data.iter().map(|l| l.split(",").collect()).collect();
        let data: Vec<Vec<i64>> = data.iter().map(|l| l.iter().filter_map(|s| s.parse().ok()).collect()).collect();
        match  data.iter().map(|l| l.len()).collect::<Vec<usize>>().windows(2).all(|w| w[0] == w[1]) {
            true => Ok(Matrix {
                rows: data.len(),
                cols: data.iter().next().unwrap_or(&vec![]).len(),
                data,

            }),
            false => Err(From::from("failed to parse array"))
        }
    }
}

impl Add for Matrix {
    type Output = Result<Matrix, Box<dyn Error>>;

    fn add(self, rhs: Self) -> Self::Output {
        match self.rows == rhs.rows && self.cols == rhs.cols {
            true => {
                Ok(Matrix {
                    data: self.data.iter().zip(rhs.data.iter()).map(|(a,b)| a.iter().zip(b.iter()).map(|(a,b)| a+b).collect()).collect(),
                    rows: self.rows,
                    cols: self.cols,
                })
            },
            false => Err(From::from("failed to add matrices"))
        }
    }
}

impl Mul for Matrix {
    type Output = Result<Matrix, Box<dyn Error>>;

    fn mul(self, rhs: Self) -> Self::Output {
        match self.cols == rhs.rows {
            true => {
                let mut data: Vec<Vec<i64>> = vec![];
                for i in 0..self.rows {
                    let mut line: Vec<i64> = vec![];
                    for j in 0..rhs.cols {
                        let mut sum = 0;
                        for k in 0..rhs.rows {
                            sum += self.data[i][k] * rhs.data[k][j];
                        }
                        line.push(sum);
                    }
                    data.push(line);
                }
                Ok(Matrix{
                    data,
                    rows: self.rows,
                    cols: rhs.cols,
                })
            },
            false => Err(From::from("failed to multiply matrices"))
        }
    }
}

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut s = self.data.iter().map(|line| {
            let mut s = line.iter().fold(String::new(), |s, d| format!("{}{},", s, d));
            s.pop();
            s
        }).fold(String::new(), |s, d| format!("{}{};", s, d));
        s.pop();
        write!(f, "{:?}", s)

    }
}