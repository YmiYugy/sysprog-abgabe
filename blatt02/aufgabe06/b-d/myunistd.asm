global myread
global mywrite
global myclose
global mypipe
global mydub2
global myfork
global myexecve
global mywait4
global mychdir
global myexit

section .text
myread:
    mov rax, 0
    syscall
    ret

mywrite:
    mov rax, 1
    syscall
    ret

myclose:
    mov rax, 3
    syscall
    ret

mypipe:
    mov rax, 22
    syscall
    ret

mydub2:
    mov rax, 33
    syscall
    ret

myfork:
    mov rax, 57
    syscall
    ret

myexecve:
    mov rax, 59
    syscall
    ret

mywait4:
    mov rax, 61
    mov r10, rcx
    syscall
    ret

mychdir:
    mov rax, 80
    syscall
    ret

myexit:
    mov rax, 231
    syscall
    ret