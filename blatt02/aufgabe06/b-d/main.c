#include <stdlib.h>
#include <string.h>
#include "myunistd.h"

int main() {
    char* buffer = (char*)malloc(sizeof(char)*512);
    memset(buffer, 0, 512);
    myread(1, buffer, 512);
    mywrite(1, buffer, strlen(buffer));
    free(buffer);
    return 0;
}