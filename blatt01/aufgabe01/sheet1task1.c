#include <stdio.h>
#include <stdlib.h>
#include "mystring.h"

int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "pass two strings as arguments!\n");
    return EXIT_FAILURE;
  }
  char *h = argv[1];
  char *j = argv[2];
  printf("the first string is %ld characters long\n", mystrlen(h));
  size_t length = mystrlen(h) + mystrlen(j) + 1;
  char *dst = (char *)malloc(sizeof(char) * length);
  mystrcpy(dst, h);
  mystrcat(dst, j);
  printf("first and second string together forever: %s\n", dst);
  free(dst);
  int cmp = mystrcmp(h, j);
  if (cmp > 0) {
    printf("the first one is 'bigger'\n");
  } else if (cmp < 0) {
    printf("the second one is 'bigger'\n");
  } else {
    printf("both are equal\n");
  }
  char* h2 = (char*)malloc(sizeof(char)* mystrlen(h)+1);
  mymemcpy(h2, h, mystrlen(h));
  h2[mystrlen(h)] = 0;
  printf("copied h to h2: %s\n", h2);
  mymemset(h2, 'a', mystrlen(h));
  printf("repalced h2 with a: %s\n", h2);
  free(h2);
  return EXIT_SUCCESS;
}
