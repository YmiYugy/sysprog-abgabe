#ifndef MYSTRING_H
#define MYSTRING_H

#include "mystddef.h"

size_t mystrlen(const char *str) {
  size_t i = 0;
  for (i = 0; str[i] != '\0'; i++)
    ;
  return i;
}

char *mystrcpy(char *dst, const char *src) {
  size_t i;
  size_t src_len = mystrlen(src);
  for (i = 0; i <= src_len; i++) {
    dst[i] = src[i];
  }

  return dst;
}

char *mystrcat(char *dst, const char *src) {
  mystrcpy(dst + mystrlen(dst), src);
  return dst;
}

int mystrcmp(const char *s1, const char *s2) {
  while (*s1 != '\0' || *s2 != '\0') {
    if (*s1 > *s2) {
      return 1;
    } else if (*s2 > *s1) {
      return -1;
    }
    s1++;
    s2++;
  }
  return 0;
}

void *mymemcpy(void* dest, void* src, size_t n){
  char* cdest = (char*)dest;
  char* csrc  = (char*)src; 
  size_t i;
  for(i = 0; i < n; i++){
    cdest[i] = csrc[i];
  }
  return dest;
}

void *mymemset(void* buf, int ch, size_t n) {
  char* cbuf = (char*)buf;
  size_t i = 0;
  for(i = 0; i < n; i++){
    cbuf[i] = (char)ch;
  }
  return buf;
}

#endif
